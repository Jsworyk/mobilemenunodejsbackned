//Load Dependencies
const helmet = require ('helmet');
const morgan = require ('morgan');
const express = require ('express');
const app = express ();
const mongoose = require ('mongoose');
const config = require ('config');

//Register API Routes
const users = require ('./routes/users');
const auth = require ('./routes/auth');
const restaurants = require ('./routes/restaurants');
const review = require ('./routes/review');
const order = require ('./routes/orders');
const payment = require ('./routes/payments');
const waiter = require ('./routes/waiter');
const kitchen = require ('./routes/kitchen');

mongoose
  .connect ('mongodb://localhost/mobilemenu')
  .then (() => console.log ('Connected to MongoDB'))
  .catch (err => {
    console.log ('NOT CONNECTING... ' + err);
  });

//Register Middleware here
app.use (express.json ());
app.use (helmet ());
app.use (express.urlencoded ({extended: true}));
if (app.get ('env') === 'development') {
  app.use (morgan ('tiny'));
  console.log ('Morgan enabled...');
}
app.use ('/uploads', express.static ('uploads'));

app.use ('/api/users', users);
app.use ('/api/auth', auth);
app.use ('/api/restaurants', restaurants);
app.use ('/api/reviews', review);
app.use ('/api/order', order);
app.use ('/api/pay', payment);
app.use ('/api/waiter', waiter);
app.use ('/api/kitchen', kitchen);

const port = process.env.PORT || 8000;
app.listen (port, () => console.log (`Listening on Port ${port}...`));
