const jwt = require ('jsonwebtoken');

//Verify JWT TOKEN

module.exports = function auth (req, res, next) {
  const token = req.header ('Authorization');
  if (!token)
    return res.status (401).send ('Access denied. No token provided.');
  try {
    const decoded = jwt.verify (token, 'MySecureKey');
    req.user = decoded;
    next ();
  } catch (err) {
    res.status (400).send ('Invalid or expired token.');
  }
};
