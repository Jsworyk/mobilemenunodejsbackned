const {Order} = require ('../models/order');
const {Restaurant} = require ('../models/restaurant');
const express = require ('express');
const auth = require ('../middleware/auth');
const router = express.Router ();

//Create Order

router.post ('/', auth, async (req, res) => {
  let order = new Order ({
    table: req.body.table,
    restaurant: req.body.restaurant,
    user: req.user,
  });
  order.save ();
  res.send (order);
});

//Add Item To Order

router.post ('/:id/restaurant/:res/item/:item', auth, async (req, res) => {
  let order = await Order.findOne ({_id: req.params.id});
  let restaurant = await Restaurant.findOne ({_id: req.params.res});
  let menu = restaurant.menu;
  for (var i = 0; i < menu.length; i++) {
    if (menu[i]._id.toString () === req.params.item) {
      order.items.push (menu[i]);
    }
  }
  Order.find ().populate ('restaurant');
  order.save ();
  res.send (order);
});

//Remove Item From Order

router.delete ('/:id/restaurant/:res/item/:item', auth, async (req, res) => {
  let order = await Order.findOne ({_id: req.params.id});
  let restaurant = await Restaurant.findOne ({_id: req.params.res});
  let menu = restaurant.menu;
  let order_items = order.items;
  for (var i = 0; i < menu.length; i++) {
    if (menu[i]._id.toString () === req.params.item) {
      order_items.splice (menu[i], 1);
      restaurant.open_orders.splice (menu[i], 1);
      console.log ('ITEM MATCH');
    }
  }
  restaurant.save ();
  order.save ();
  res.send (order);
});

//Mark Order Ready For Delivery

router.patch ('/:id', auth, async (req, res) => {
  let order = await Order.findOne ({_id: req.params.id});
  order.status = req.body.status;
  order.save ();
  res.send (order);
});

//Get Order By ID

router.get ('/byId/:id', auth, async (req, res) => {
  let order = await Order.find ({_id: req.params.id});
  res.send (order);
});

//Get Orders By Restaurant

router.get ('/:id', auth, async (req, res) => {
  let order = await Order.find ({restaurant: req.params.id});
  res.send (order);
});

module.exports = router;
