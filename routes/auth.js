const {User} = require ('../models/user');
const {Restaurant} = require ('../models/restaurant');
const {Waiter} = require ('../models/waiter');
const express = require ('express');
const _ = require ('lodash');
const bcrypt = require ('bcrypt');
const Joi = require ('joi');
const jwt = require ('jsonwebtoken');
const config = require ('config');
const router = express.Router ();

router.post ('/', async (req, res) => {
  const {error} = validate (req.body);
  if (error)
    return res.status (400).send ('Error: ' + error.details[0].message);
  let user = await User.findOne ({email: req.body.email});
  if (!user) return res.status (400).send ('Invalid Email or Password.');
  const validPassword = await bcrypt.compare (req.body.password, user.password);
  if (!validPassword)
    return res.status (400).send ('Invalid Email or Password.');

  const token = jwt.sign ({user: user}, 'MySecureKey');

  res.send ({
    user: user,
    token_type: 'Bearer ',
    access_token: token,
  });
});

router.post ('/restaurant', async (req, res) => {
  let restaurant = await Restaurant.findOne ({email: req.body.email});
  if (!restaurant) return res.status (400).send ('Invalid Email or Password.');
  const validPassword = await bcrypt.compare (
    req.body.password,
    restaurant.password
  );
  if (!validPassword)
    return res.status (400).send ('Invalid Email or Password.');
  const token = jwt.sign ({restaurant: restaurant}, 'MySecureKey');
  res.send ({
    restaurant: restaurant,
    token_type: 'Bearer ',
    access_token: token,
  });
});

router.post ('/waiter', async (req, res) => {
  let waiter = await Waiter.findOne ({email: req.body.email});
  if (!waiter) return res.status (400).send ('Invalid Email Or Password');
  const validPassword = await bcrypt.compare (
    req.body.password,
    waiter.password
  );
  if (!validPassword)
    return res.status (400).send ('Invalid Email Or Password');
  const token = jwt.sign ({waiter: waiter}, 'MySecureKey');
  res.send ({
    waiter: waiter,
    token_type: 'Bearer ',
    access_token: token,
  });
});

router.post ('/kitchen', async (req, res) => {
  let waiter = await Waiter.findOne ({email: req.body.email});
  if (!waiter) return res.status (400).send ('Invalid Email Or Password');
  const validPassword = await bcrypt.compare (
    req.body.password,
    waiter.password
  );
  if (!validPassword)
    return res.status (400).send ('Invalid Email Or Password');
  const token = jwt.sign ({waiter: waiter}, 'MySecureKey');
  res.send ({
    waiter: waiter,
    token_type: 'Bearer ',
    access_token: token,
  });
});

function validate (req) {
  const schema = {
    email: Joi.string ().min (5).max (255).required ().email (),
    password: Joi.string ().min (5).max (255).required (),
  };

  return Joi.validate (req, schema);
}

module.exports = router;
