const {User, validate} = require ('../models/user');
const express = require ('express');
const _ = require ('lodash');
const bcrypt = require ('bcrypt');
const router = express.Router ();
const jwt = require ('jsonwebtoken');
const auth = require ('../middleware/auth');
const multer = require ('multer');
const storage = multer.diskStorage ({
  destination: function (req, file, cb) {
    cb (null, './uploads/');
  },
  filename: function (req, file, cb) {
    cb (null, new Date ().toISOString () + file.originalname);
  },
});
const upload = multer ({storage: storage});

//Get Current User

router.get ('/', auth, async (req, res) => {
  const user = await User.findById (req.user.user._id);
  console.log (user);
  res.send (user);
});

//Get User By ID

router.get ('/:id', async (req, res) => {
  const user = await User.findById ({_id: req.params.id});
  res.send (user);
});

//Create New User

router.post ('/', async (req, res) => {
  console.log (req.file);
  const {error} = validate (req.body);
  if (error) return res.status (400).send (error.details[0].message);

  let user = await User.findOne ({email: req.body.email});
  if (user) return res.status (400).send ('User Already Registered.');

  user = new User (req.body);
  const salt = await bcrypt.genSalt (10);
  user.password = await bcrypt.hash (user.password, salt);
  await user.save ();

  const token = user.generateAuthToken ();
  res.header ('x-auth-token', token).send ({
    user: user,
    access_token: token,
  });
});

//Follow Other User

router.post ('/:id/follow', auth, async (req, res) => {
  let user = await User.findOne ({_id: req.params.id});
  if (!user)
    res.status (400).send ('A user with the provided ID could not be found');
  let followers = user.followers;
  let found = followers.some (function (o) {
    return o['_id'] === req.user.user._id;
  });
  if (!found) {
    followers.push (req.user.user);
  }
  user.save ();
  res.send (user);
});

//Unfollow other user

router.delete ('/:id/follow', auth, async (req, res) => {
  let user = await User.findOne ({_id: req.params.id});
  if (!user)
    res.status (400).send ('A User With The Provided ID Could Not Be Found');
  let followers = user.followers;
  for (var i = 0; i < followers.length; i++) {
    if (followers[i]._id.toString () === req.user.user._id) {
      followers.splice (i, 1);
      break;
    }
  }
  user.save ();
  res.send (user);
});

module.exports = router;
