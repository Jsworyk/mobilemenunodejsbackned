const {Waiter} = require ('../models/waiter');
const {Restaurant} = require ('../models/restaurant');
const express = require ('express');
const _ = require ('lodash');
const bcrypt = require ('bcrypt');
const router = express.Router ();
const jwt = require ('jsonwebtoken');
const auth = require ('../middleware/auth');
const multer = require ('multer');
const storage = multer.diskStorage ({
  destination: function (req, file, cb) {
    cb (null, './uploads/');
  },
  filename: function (req, file, cb) {
    cb (null, new Date ().toISOString () + file.originalname);
  },
});
const upload = multer ({storage: storage});

router.get ('/self', auth, async (req, res) => {
  let waiter = await Waiter.findOne ({_id: req.user.waiter._id});
  res.send (waiter);
});

router.post ('/', async (req, res) => {
  let waiter = await Waiter.findOne ({email: req.body.email});
  if (waiter) return res.status (400).send ('Waiter Already Registered.');
  let restaurant = await Restaurant.findOne ({
    _id: req.body.restaurant_identifier,
  });
  if (!restaurant)
    return res
      .status (405)
      .send ('No Restaurant Found Tethered To This Restaurant Identifier');

  waiter = new Waiter (req.body);
  const salt = await bcrypt.genSalt (10);
  waiter.password = await bcrypt.hash (waiter.password, salt);
  await waiter.save ();

  const token = waiter.generateAuthToken ();
  res.header ('x-auth-token', token).send ({
    waiter: waiter,
    access_token: token,
  });
});

router.get ('/', async (req, res) => {
  let waiter = await Waiter.find ();
  res.send (waiter);
});

module.exports = router;
