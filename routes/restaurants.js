const {Restaurant, validate, SectionSchema} = require ('../models/restaurant');
const express = require ('express');
const _ = require ('lodash');
const bcrypt = require ('bcrypt');
const router = express.Router ();
const jwt = require ('jsonwebtoken');
const Joi = require ('joi');
const auth = require ('../middleware/auth');
const multer = require ('multer');
const mongoose = require ('mongoose');
const {User} = require ('../models/user');
const storage = multer.diskStorage ({
  destination: function (req, file, cb) {
    cb (null, './uploads/');
  },
  filename: function (req, file, cb) {
    cb (null, new Date ().toISOString () + file.originalname);
  },
});
const upload = multer ({storage: storage});

router.get ('/self', auth, async (req, res) => {
  const restaurant = await Restaurant.findById (req.user.restaurant._id);
  console.log (req.user.restaurant._id);
  res.send (restaurant);
});

// Get All Restaurants

router.get ('/', async (req, res) => {
  const restaurant = await Restaurant.find ().sort ('name').populate ('author');
  res.send (restaurant);
});

// Get Restaurant By ID

router.get ('/:id', async (req, res) => {
  const restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (404).send ('Restaurant Could Not Be Found');
    return;
  }
  res.send (restaurant);
});

//Get Orders By Restarant

router.get ('/:id/orders', async (req, res) => {
  const restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (404).send ('Restaurant with given ID could not be found');
    return;
  }
  let orders = restaurant.open_orders;
  res.send (orders);
});

// Get Menu By Restaurant ID

router.get ('/:id/menu', async (req, res) => {
  const restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (404).send ('Restaurant Could Not Be Found');
    return;
  }
  res.send ({menu: restaurant.menu});
});

//Get Menu Items By Category Value

router.get ('/:id/menu_by_category/:category', async (req, res) => {
  const restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (400).send ('Restaurant Could Not Be Found');
    return;
  }
  const menu = restaurant.menu;
  const menu_by_category = [];
  for (var i = 0; i < menu.length; i++) {
    if (menu[i].category == req.params.category) {
      menu_by_category.push (menu[i]);
    }
  }
  res.send (menu_by_category);
});

// Update Restaurant Description

router.put ('/:id/description', async (req, res) => {
  const restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (404).send ('Restaurant Could Not Be Found');
    return;
  }
  restaurant.description = req.body.description;
  restaurant.save ();
  res.send (restaurant);
});

// Update Restaurant Category

router.put ('/:id/category', async (req, res) => {
  const restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (404).send ('Restaurant Could Not Be Found');
    return;
  }

  restaurant.category = req.body.category;
  restaurant.save ();
  res.send (restaurant);
});

// Update Restaurant Location

router.put ('/:id/location', async (req, res) => {
  const restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (404).send ('Restaurant Could Not Be Found');
    return;
  }

  restaurant.location = req.body.location;
  restaurant.save ();
  res.send (restaurant);
});

//Update Restaurant Photo

router.patch (
  '/:id/photo',
  upload.single ('restaurantPhoto'),
  async (req, res) => {
    const restaurant = await Restaurant.findOne ({_id: req.params.id});
    if (!restaurant) {
      res
        .status (400)
        .send ('A Restaurant With The Given ID Could Not Be Found');
      return;
    }
    restaurant.restaurantPhoto = req.file.path;
    restaurant.save ();
    res.send (restaurant);
  }
);

//Add Item To Menu

router.put ('/:id/add_item', async (req, res) => {
  const restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (400).send ('A Restaurant With The Given ID Could Not Be Found');
    return;
  }

  const menuItem = req.body;
  restaurant.menu.push (menuItem);
  restaurant.save ();
  res.send (restaurant);
});

//Add Drink To Menu

router.post ('/:id/add_drink', async (req, res) => {
  const restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (400).send ('A Restaurant With The Given ID Could Not Be Found');
    return;
  }
  const drink = req.body;
  restaurant.drinks.push (drink);
  restaurant.save ();
  res.send (restaurant);
});

// Get Drinks By Restaurant ID

router.get ('/:id/drinks', async (req, res) => {
  const restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (404).send ('Restaurant Could Not Be Found');
    return;
  }
  res.send ({menu: restaurant.drinks});
});

//Delete Menu Item

router.delete ('/:id/item/:item', async (req, res) => {
  const restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (400).send ('A restaurant with the given ID could not be found');
    return;
  }
  let menu = restaurant.menu;
  for (var i = 0; i < menu.length; i++) {
    if (menu[i]._id.toString () === req.params.item) {
      menu.splice (i, 1);
    }
  }
  restaurant.save ();
  res.send (menu);
});

//Review Menu Item By Restaurant ID and item ID

router.put ('/:id/review/:item', auth, async (req, res) => {
  const restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (400).send ('A Restaurant With The Given ID Could Not Be Found');
    return;
  }
  let menu = restaurant.menu;
  for (var i = 0; i < menu.length; i++) {
    if (menu[i]._id.toString () === req.params.item) {
      menu[i].reviews.push ({
        review: req.body.review,
        rating: req.body.rating,
        author: req.user.user,
      });
      rating += rating;
    }
  }
  console.log (req.user.user);
  Restaurant.find ().populate ('author');
  await restaurant.save ();
  res.send (restaurant);
});

//Update Restaurant Item Image By Restaurant ID and Item ID

router.patch (
  '/:id/add_image/:item',
  upload.single ('itemImage'),
  async (req, res) => {
    const restaurant = await Restaurant.findOne ({_id: req.params.id});
    if (!restaurant) {
      res
        .status (400)
        .send ('A Restaurant With The Given ID Could Not Be Found');
      return;
    }
    await Restaurant.update (
      {'menu._id': req.params.item},
      {
        $set: {
          'menu.$.itemImage': req.file.path,
        },
      }
    );
    restaurant.save ();
    res.send (restaurant);
  }
);

//Update Restaurant Item By Restaurant Id and Item ID

router.patch ('/:id/edit_item/:item', async (req, res) => {
  const restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (400).send ('A Restaurant With The Given ID Could Not Be Found');
    return;
  }
  Restaurant.update (
    {'menu._id': req.params.item},
    {
      $set: {
        'menu.$.price': req.body.price,
        'menu.$.name': req.body.name,
        'menu.$.description': req.body.description,
        'menu.$.category': req.body.category,
      },
    },
    err => {
      if (err) {
        console.log (err.message);
      }
    }
  );
  restaurant.save ();
  res.send (restaurant);
});

//Get Menu By Restaurant ID

router.get ('/:id/menu', async (req, res) => {
  const restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (400).send ('A Restaurant With The Given ID Could Not Be Found');
    return;
  }
  res.send (restaurant.menu);
});

// Add Restaurant To Database

router.post ('/', async (req, res) => {
  let restaurant = await Restaurant.findOne ({email: req.body.email});
  if (restaurant) {
    return res
      .status (400)
      .send ('A Restaurant With This Email Address Is Already Registered.');
  }
  restaurant = new Restaurant (req.body);
  const salt = await bcrypt.genSalt (10);
  restaurant.password = await bcrypt.hash (restaurant.password, salt);
  await restaurant.save ();

  const token = restaurant.generateAuthToken ();
  res.header ('x-auth-token', token).send ({
    restaurant: restaurant,
    access_token: token,
  });
});

module.exports = router;
