const {Kitchen} = require ('../models/kitchen');
const {Restaurant} = require ('../models/restaurant');
const express = require ('express');
const _ = require ('lodash');
const bcrypt = require ('bcrypt');
const router = express.Router ();
const jwt = require ('jsonwebtoken');
const auth = require ('../middleware/auth');
const multer = require ('multer');
const storage = multer.diskStorage ({
  destination: function (req, file, cb) {
    cb (null, './uploads/');
  },
  filename: function (req, file, cb) {
    cb (null, new Date ().toISOString () + file.originalname);
  },
});
const upload = multer ({storage: storage});

router.get ('/self', auth, async (req, res) => {
  let kitchen = await Kitchen.findOne ({_id: req.user.kitchen._id});
  res.send (kitchen);
});

router.post ('/', async (req, res) => {
  let kitchen = await Kitchen.findOne ({email: req.body.email});
  if (kitchen) return res.status (400).send ('Waiter Already Registered.');
  let restaurant = await Restaurant.findOne ({
    _id: req.body.restaurant_identifier,
  });
  if (!restaurant)
    return res
      .status (405)
      .send ('No Restaurant Found Tethered To This Restaurant Identifier');

  kitchen = new Kitchen (req.body);
  const salt = await bcrypt.genSalt (10);
  kitchen.password = await bcrypt.hash (kitchen.password, salt);
  await kitchen.save ();

  const token = kitchen.generateAuthToken ();
  res.header ('x-auth-token', token).send ({
    kitchen: kitchen,
    access_token: token,
  });
});

router.get ('/', async (req, res) => {
  let kitchen = await Kitchen.find ();
  res.send (kitchen);
});

module.exports = router;
