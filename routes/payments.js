const express = require ('express');
const auth = require ('../middleware/auth');
const router = express.Router ();
var braintree = require ('braintree');
const {Card} = require ('../models/card');
var gateway = braintree.connect ({
  environment: braintree.Environment.Sandbox,
  merchantId: 'bcv2gkrvtpbq5j6k',
  publicKey: 'dn557vr6wsdxvc7h',
  privateKey: '77af7ff83f471bee12dfa2e68717ec24',
});

//Add Credit Card To System
router.post ('/creditcard', auth, async (req, res) => {
  let card = new Card ({
    card_holder: req.body.card_holder,
    card_number: req.body.card_number,
    expiration: req.body.expiration,
    cvv: req.body.cvv,
    id: req.user.user._id.toString (),
  });
  console.log (req.user.user._id);
  await card.save ();
  res.send (card);
});

//Get Credit Cards By ID
router.get ('/creditcard/:id', auth, async (req, res) => {
  let card = await Card.find ();
  let cards = [];
  for (var i = 0; i < card.length; i++) {
    if (card[i].id === req.params.id) {
      cards.push (card[i]);
    }
  }
  res.send (card);
});

//Get Credit Cards In System
router.get ('/creditcard', async (req, res) => {
  let card = await Card.find ();
  res.send (card);
});

//Issue BrainTree API Token
router.get ('/braintree_token', auth, async (req, res) => {
  gateway.clientToken.generate ({}, (err, response) => {
    res.send (response.clientToken);
  });
});

//Process Payments Using BrainTree
router.post ('/checkout', auth, async (req, res) => {
  var nonceFromTheClient = req.body.payment_method_nonce;
  gateway.transaction.sale (
    {
      amount: '10.00',
      paymentMethodNonce: nonceFromTheClient,
      options: {
        submitForSettlement: true,
      },
    },
    function (err, result) {
      if (err) {
        console.log (err.message);
      } else {
        console.log (res.send (result));
      }
    }
  );
});

module.exports = router;
