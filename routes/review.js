const {Review} = require ('../models/review');
const {Restaurant} = require ('../models/restaurant');
const express = require ('express');
const auth = require ('../middleware/auth');
const router = express.Router ();

//Leave Review On Restaurant

router.post ('/:id', auth, async (req, res) => {
  let review = new Review ({
    author: req.user.user,
    content: req.body.content,
    rating: req.body.rating,
    restaurant: req.params.id,
  });
  let restaurant = await Restaurant.findOne ({_id: req.params.id});
  restaurant.restaurant_review.push (review);
  restaurant.rating += req.body.rating;
  console.log (req.user);
  Review.find ().populate ('author');
  review.save ();
  restaurant.save ();
  res.send (restaurant);
});

//Review Menu Item

router.post ('/:id/item/:item', auth, async (req, res) => {
  let review = new Review ({
    content: req.body.content,
    rating: req.body.rating,
    author: req.user.user,
  });
  let restaurant = await Restaurant.findOne ({_id: req.params.id});
  console.log (req.user);
  Review.find ().populate ('author');
  review.save ();
  let menu = restaurant.menu;
  for (var i = 0; i < menu.length; i++) {
    if (req.params.item === menu[i]._id.toString ()) {
      menu[i].reviews.push (review);
    }
  }
  Restaurant.find ().populate ('author');
  restaurant.save ();
  res.send (restaurant);
});

//Get Reviews By Item ID

router.get ('/:id/item/:item', auth, async (req, res) => {
  let restaurant = await Restaurant.findOne ({_id: req.params.id});
  if (!restaurant) {
    res.status (404).send ('Restaurant Could Not Be Found');
    return;
  }
  let menu = restaurant.menu;
  for (var i = 0; i < menu.length; i++) {
    if (req.params.item === menu[i]._id.toString ()) {
      res.send (menu[i]);
      return;
    } else {
      res.send ('NO DICE');
    }
  }
});

//Get Reviews By Restaurant ID

router.get ('/:id', auth, async (req, res) => {
  let review = await Review.find ();
  let rest_rev = [];
  for (var i = 0; i < review.length; i++) {
    if (review[i].restaurant === req.params.id) {
      rest_rev.push (review[i]);
    }
  }
  res.send (rest_rev);
});

//Delete Review By Review Id

router.delete ('/:id', auth, async (req, res) => {
  Review.findByIdAndDelete (req.params.id, (err, todo) => {
    if (err) return res.status (500).send (err.message);
  });
  res.send ('DONE');
});

//Like Review

router.patch ('/:res/like/:id', auth, async (req, res) => {
  let review = await Review.findOne ({_id: req.params.id});
  if (!review)
    res.status (400).send ('A review with the provided ID Could not be found');
  if (review.likers.indexOf (req.user.user._id) === -1) {
    review.likers.push (req.user.user._id);
  } else {
    res.send ({res: 'YOU HAVE ALREADY LIKED THIS', review: review});
  }
  review.save ();
  res.send (review);
});

router.delete ('/:id/like', auth, async (req, res) => {
  let review = await Review.findOne ({_id: req.params.id});
  if (!review)
    res.status (400).send ('A review with the provided ID Could not be found');
  let likes = review.likers;
  for (var i = 0; i < likes.length; i++) {
    if (likes[i]._id === req.user.user._id) {
      likes.splice (i, 1);
    }
  }
  review.save ();
  res.send (review);
});

//Get ALL Reviews

router.get ('/', async (req, res) => {
  const review = await Review.find ();
  res.send (review);
});

//Get Review By User ID

router.get ('/:id/user', async (req, res) => {
  const review = await Review.find ();
  let rest_review = [];
  for (var i = 0; i < review.length; i++) {
    if (review[i].author._id.toString () === req.params.id.toString ()) {
      rest_review.push (review[i]);
    }
  }
  res.send (rest_review);
});

// router.get ('/:id/user', async (req, res) => {
//   const review = await Review.find ();
//   res.send (review);
// });

module.exports = router;
