const mongoose = require ('mongoose');
const jwt = require ('jsonwebtoken');

const reviewSchema = new mongoose.Schema ({
  author: {
    type: Object,
    ref: 'User',
  },
  content: {
    type: String,
    default: null,
  },
  rating: {
    type: Number,
    default: null,
  },
  date_posted: {
    type: Date,
    defaault: null,
  },
  likers: [
    {
      type: Object,
      ref: 'User',
      default: null,
    },
  ],
  restaurant: {
    type: String,
    default: null,
  },
});

const Review = mongoose.model ('Review', reviewSchema);

exports.Review = Review;
