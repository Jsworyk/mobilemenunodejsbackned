const mongoose = require ('mongoose');
const jwt = require ('jsonwebtoken');

const KitchenSchema = new mongoose.Schema ({
  name: {
    type: String,
    required: true,
    minLength: 1,
    maxLength: 50,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  profilePhoto: {
    type: String,
    required: false,
    default: '/uploads/default_avatar.png',
  },
  restaurant_identifier: {
    type: String,
    required: true,
  },
});

KitchenSchema.methods.generateAuthToken = function () {
  const token = jwt.sign ({user: this}, 'MySecureKey');
  return token;
};

const Kitchen = mongoose.model ('Kitchen', KitchenSchema);

exports.Kitchen = Kitchen;
exports.KitchenSchema = KitchenSchema;
