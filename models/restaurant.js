const mongoose = require ('mongoose');
const Joi = require ('joi');
const jwt = require ('jsonwebtoken');
const {Review} = require ('./review');

const MenuItem = new mongoose.Schema ({
  name: {
    type: String,
    required: false,
    minlength: 1,
    maxlength: 150,
  },
  description: {
    type: String,
    required: false,
    default: null,
  },
  price: {
    type: Number,
    required: false,
    default: null,
  },
  category: {
    type: String,
    required: false,
    default: null,
  },
  reviews: [
    {
      content: {
        type: String,
        required: false,
        default: '',
      },
      rating: {
        type: Number,
      },
      author: {
        type: Object,
        ref: 'User',
      },
    },
  ],
  itemImage: {
    type: String,
    required: false,
    default: 'uploads/default_avatar.png',
  },
});

const restaurantSchema = new mongoose.Schema ({
  name: {
    type: String,
    required: true,
    minlength: 1,
    maxlength: 150,
  },
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 1024,
  },
  description: {
    type: String,
    required: false,
    default: null,
  },
  category: {
    type: String,
    required: false,
    default: null,
  },
  location: {
    type: String,
    required: false,
    default: null,
  },
  menu: [MenuItem],
  drinks: [MenuItem],
  restaurantPhoto: {
    type: String,
    required: false,
    default: 'uploads/default_avatar.png',
  },
  restaurant_review: [
    {
      type: Object,
      ref: 'Review',
    },
  ],
  open_orders: [
    {
      type: Object,
      ref: 'Orders',
    },
  ],
  rating: {
    type: Number,
    default: 0,
  },
});

restaurantSchema.methods.generateAuthToken = function () {
  const token = jwt.sign ({restaurant: this}, 'MySecureKey');
  return token;
};

const Restaurant = mongoose.model ('Restaurant', restaurantSchema);
const Menu = mongoose.model ('MenuItem', MenuItem);

function validateRestaurant (restaurant) {
  const schema = {
    name: Joi.string ().min (5).max (50).required (),
    email: Joi.string ().min (5).max (255).required ().email (),
    password: Joi.string ().min (5).max (255).required (),
  };
  return Joi.validate (restaurant, schema);
}

exports.Restaurant = Restaurant;
exports.Menu = Menu;
exports.validate = validateRestaurant;
