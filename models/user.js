const mongoose = require ('mongoose');
const Joi = require ('joi');
const jwt = require ('jsonwebtoken');
const fs = require ('fs');

const userSchema = new mongoose.Schema ({
  name: {
    type: String,
    required: true,
    minLength: 1,
    maxlength: 50,
  },
  email: {
    type: String,
    required: true,
    minlengh: 5,
    maxlength: 255,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 1024,
  },
  profilePhoto: {
    type: String,
    required: false,
    default: 'uploads/default_avatar.png',
  },
  followers: [
    {
      type: Object,
      ref: 'User',
    },
  ],
});

userSchema.methods.generateAuthToken = function () {
  const token = jwt.sign ({user: this}, 'MySecureKey');
  return token;
};

const User = mongoose.model ('User', userSchema);

function validateUser (user) {
  const schema = {
    name: Joi.string ().min (5).max (50).required (),
    email: Joi.string ().min (5).max (255).required ().email (),
    password: Joi.string ().min (5).max (255).required (),
  };
  return Joi.validate (user, schema);
}

exports.User = User;
exports.userSchema = userSchema;
exports.validate = validateUser;
