const mongoose = require ('mongoose');

const card_schema = new mongoose.Schema ({
  card_holder: {
    type: String,
    required: true,
    default: null,
  },
  card_number: {
    type: Number,
    required: true,
    default: null,
  },
  expiration: {
    type: Number,
    required: true,
    default: null,
  },
  cvv: {
    type: Number,
    required: true,
    default: null,
  },
  id: {
    type: String,
    required: true,
    default: null,
  },
});

const Card = mongoose.model ('Card', card_schema);

exports.Card = Card;
