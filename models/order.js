const mongoose = require ('mongoose');

const orderSchema = new mongoose.Schema ({
  items: [
    {
      type: Object,
      default: null,
    },
  ],
  table: {
    type: Number,
  },
  user: {
    type: Object,
    ref: 'User',
  },
  restaurant: {
    type: Object,
    ref: 'Restaurant',
  },
  status: {
    type: Number,
    default: 0,
  },
});

const Order = mongoose.model ('Order', orderSchema);

exports.Order = Order;
